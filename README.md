# README #

### Зачем ###

* Поиграть с asyncio
* Попробовать стек, не связанный с Django: Fastapi-Pydantic-Gino

### Что Где ###

* /web - создание заданий на прокачку данных о погоде с https://openweathermap.org/api и просмотр результатов
* /worker - выполнятель бэкграундных тасок
* /common - Схемы Pydantic, модели Gino, общий Dockerfile 

### Как с этим жить ###

* для полноценной работы необходим файл ```.env``` (создайте таковой и скопируйте в него содержимое файла ```env```) в корне проекта - из него композ берет аутентификацию постгреса и АПИ-ключ погодного сервиса
* Чтобы просто запустить:
  
    ```docker-compose --env-file ./env up -d```

* Чтобы запустить c возможностью дебага (контейнеры поднимутся, при этом самми сервисы не будут запущены - руками через дебагер):
  
    ```docker-compose --env-file ./env -f ./docker-compose.yml -f ./docker-compose.local.yml up -d```

### worker ###

* Количество параллельных закачек ограничено константой **MAX_CONCURRENT_TASKS_LIMIT**
* На ключевую функцию - **consume** есть юнит-тесты в /worker/tests. Запускаются изнутри контейнера:

    ```python -m pytest tests```

### web ###

* Все видно в свагере <http://127.0.0.1:8000/docs#>, но..
* Для начала нужно создать пользователя через эндпоинты auth
* /create_job/ - создает задачу на прокачку данных

```curl -X POST "http://127.0.0.1:8000/create_job/" -H  "accept: application/json" -H  "Content-Type: application/json" -d "[\"Moscow\",\"qwewsdadsawe\"]"```

```javascript
{
    "user_id": "7ef4c78c-0b4c-4408-afb2-0251c19a814c",
    "job_id":"2e9a693a-9a29-40e8-ace3-78f2ae7755d4",
    "task":[
        "Moscow",
        "qwewsdadsawe"
    ]
}
```

* /jobs/ - просмотреть закачки, можно фильтровать по статусу завершенности, и пользователю, который создал таску. Делается поверхностная сериализация без выдачи непосредственных результатов для городов из списка (без джоина с данными)
    
```curl -X GET "http://127.0.0.1:8000/jobs/?completed=true" -H  "accept: application/json"```

```javascript
[
    {
        "user_id": "7ef4c78c-0b4c-4408-afb2-0251c19a814c",
        "created":"14:22:26.448531",
        "task":[
            "Moscow",
            "qwewsdadsawe"
        ],
        "completed":"14:30:57.712576",
        "id":"2e9a693a-9a29-40e8-ace3-78f2ae7755d4"
    }
]
```

* /jobs/{job_id} - подробная информация по конкретной закачке, с джойном по погоде для каждого города

```curl -X GET "http://127.0.0.1:8000/jobs/2e9a693a-9a29-40e8-ace3-78f2ae7755d4" -H  "accept: application/json"```


```javascript
{
    "user_id": "7ef4c78c-0b4c-4408-afb2-0251c19a814c",
    "created":"14:22:26.448531",
    "task":[
        "Moscow",
        "qwewsdadsawe"
    ],
    "completed":"14:30:57.712576",
    "id":"2e9a693a-9a29-40e8-ace3-78f2ae7755d4",
    "completion_rate":0.5,
    "records":[
        {
            "data":{
                "error":"code 404"
            },
            "timestamp":null,
            "id":"9f4922be-0cb8-42b5-a7a4-67e83d63442f",
            "job_id":"2e9a693a-9a29-40e8-ace3-78f2ae7755d4",
            "city_name":"qwewsdadsawe",
            "success":false
        },
        {
            "data":{
                "pressure":1016,
                "wind_speed":4,
                "temperature":289,
                "wind_direction":360
            },
            "timestamp": "18:17:43",
            "id":"05e418a8-80b9-4ada-8a6b-e95e584cb510",
            "job_id":"2e9a693a-9a29-40e8-ace3-78f2ae7755d4",
            "city_name":"Moscow",
            "success":true
        }
    ]
}
```
