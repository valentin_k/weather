import uuid
from datetime import datetime

from gino import Gino
from sqlalchemy.dialects.postgresql import JSONB, UUID


db = Gino()


class GinoBaseUserTable(db.Model):
    __tablename__ = 'users'

    id = db.Column(UUID(), primary_key=True)
    email = db.Column(db.String(), unique=True, index=True, nullable=False)
    hashed_password = db.Column(db.String(), nullable=False)
    is_active = db.Column(db.Boolean(), default=True, nullable=False)
    is_superuser = db.Column(db.Boolean(), default=False, nullable=False)


class WheatherFetchJob(db.Model):
    __tablename__ = 'jobs'

    id = db.Column(
        UUID(),
        primary_key=True,
        default=uuid.uuid4,
        unique=True,
        nullable=False
    )
    user_id = db.Column(UUID(), db.ForeignKey('users.id'))
    task = db.Column(JSONB(), nullable=False, server_default='[]')
    created = db.Column(db.Time(), nullable=False, default=datetime.now())
    completed = db.Column(db.Time())

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self._records = set()

    @property
    def records(self):
        return self._records

    @records.setter
    def add_record(self, record):
        self._records.add(record)

    @property
    def completion_rate(self):
        return len(
            [record for record in self.records if record.success]
        )/len(self.task)


class WheatherRecord(db.Model):
    __tablename__ = 'records'

    id = db.Column(
        UUID(),
        primary_key=True,
        default=uuid.uuid4,
        unique=True,
        nullable=False
    )
    job_id = db.Column(UUID(), db.ForeignKey('jobs.id'))
    success = db.Column(db.Boolean(), nullable=False)
    city_name = db.Column(db.String(), nullable=False)
    timestamp = db.Column(db.Time(), nullable=True)

    data = db.Column(JSONB(), nullable=False, server_default='{}')

    # TODO
    # Индекс лучше делать вручную, да и миграции тоже,
    # по-взрослому, через Алембик
    # вот то все что ниже - неудобно работает
    # зачем-то вместо даты все свойства выходят в корень

    # error = db.StringProperty(prop_name='data')
    # pressure = db.IntegerProperty(prop_name='data')
    # wind_speed = db.IntegerProperty(prop_name='data')
    # temperature = db.IntegerProperty(prop_name='data')
    # wind_direction = db.IntegerProperty(prop_name='data')

    # @db.declared_attr
    # def pressure_idx(cls):
    #     return db.Index('pressure_idx', cls.pressure)
