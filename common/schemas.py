from datetime import datetime
from typing import Dict, Union
from fastapi_users import models
from pydantic import BaseModel, Field, root_validator


class User(models.BaseUser):
    pass


class UserCreate(models.BaseUserCreate):
    pass


class UserUpdate(User, models.BaseUserUpdate):
    pass


class UserDB(User, models.BaseUserDB):
    pass


class WheaterRecordMain(BaseModel):
    temp: float = Field(...)
    pressure: int = Field(...)


class WheaterRecordWind(BaseModel):
    speed: float = Field(...)
    deg: int = Field(...)


class WheaterRecordAPI(BaseModel):
    name: str = Field(...)
    dt: int = Field(...)
    wind: WheaterRecordWind = Field(...)
    main: WheaterRecordMain = Field(...)


class WheaterRecordInternal(BaseModel):
    city_name: str = Field(..., alias='name')
    success: bool
    timestamp: datetime

    data: Dict[str, Union[int, float]]

    @root_validator(pre=True)
    def restruct_data(cls, values):
        values['timestamp'] = datetime.fromtimestamp(values['dt'])

        values['data'] = {
            'temperature': values['main']['temp'],
            'pressure': values['main']['pressure'],
            'wind_speed': values['wind']['speed'],
            'wind_direction': values['wind']['deg'],
        }

        return values
