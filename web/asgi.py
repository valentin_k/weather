import asyncio
import orjson
import uuid
from typing import List, Optional

import aio_pika
from fastapi import Depends, FastAPI, HTTPException, Query, WebSocket
from fastapi.encoders import jsonable_encoder
from fastapi.responses import HTMLResponse

from html import get_feeed_for_task
import settings
from common.models import WheatherFetchJob, WheatherRecord, db
from common.schemas import User
from users import add_user_routes, fastapi_users


app = FastAPI()
add_user_routes(app)


@app.on_event('startup')
async def startup_event():
    await asyncio.sleep(5)
    await db.set_bind(
        'postgresql://{role}:{password}@{host}:{port}/{database}'.format(
            **settings.POSTGRES_CREDS
        )
    )
    await db.gino.create_all()
    app.amqp_connection = await aio_pika.connect_robust(
        'amqp://{login}:{password}@{host}/'.format(**settings.AMQP_CREDS),
        loop=asyncio.get_running_loop()
    )
    app.amqp_channel = await app.amqp_connection.channel()

    app.queues = {}


@app.on_event('shutdown')
async def shutdown_event():
    await db.pop_bind().close()
    await app.amqp_channel.close()
    await app.amqp_connection.close()


@app.get('/')
async def root():
    return {'message': 'Hello World'}


@app.get('/jobs/')
async def collected_data(
    completed: Optional[bool] = Query(None, description='Filter completed'),
    my: Optional[bool] = Query(None, description='Created by me'),
    user: User = Depends(fastapi_users.get_optional_current_user)
):
    query = WheatherFetchJob.query

    if completed is not None:
        query = query.where(
            WheatherFetchJob.completed.isnot(None) if completed
            else WheatherFetchJob.completed.is_(None)
        )

    if my is not None and user is not None:
        query = query.where(
            (WheatherFetchJob.user_id != user.id) != my
        )

    return [job.to_dict() for job in await query.gino.all()]


@app.get('/jobs/{job_id}')
async def collected_data_by_id(job_id: uuid.UUID):
    query = WheatherFetchJob.outerjoin(WheatherRecord).select(
    ).where(WheatherFetchJob.id == job_id)
    loader = WheatherFetchJob.distinct(WheatherFetchJob.id).load(
        add_record=WheatherRecord
    )
    job = await query.gino.load(loader).one_or_none()

    if job is None:
        raise HTTPException(status_code=404, detail='Item not found')

    return {
        **job.to_dict(),
        'completion_rate': job.completion_rate,
        'records': [record.to_dict() for record in job.records],
    }


@app.post('/create_job/')
async def create_item(
    task: List[str],
    user: User = Depends(fastapi_users.get_current_user)
):
    job = await WheatherFetchJob.create(
        user_id=user.id,
        task=task
    )
    job_data = jsonable_encoder({'job_id': job.id, 'task': task})

    await app.amqp_channel.default_exchange.publish(
        aio_pika.Message(
            body=orjson.dumps(job_data)
        ),
        routing_key='fetch_wheather'
    )

    app.queues[job.id] = await app.amqp_channel.declare_queue(
        str(job.id),
        # удаляем очередь как только консьюмер отсоединяется
        exclusive=True,
        auto_delete=True
    )

    return {
        'feed': f'http://{settings.HOST}:8000/result_feed/{job.id}',
        **job_data
    }


@app.get("/result_feed/{job_id}")
async def feed(job_id: uuid.UUID):
    return HTMLResponse(get_feeed_for_task(job_id))


@app.websocket("/result_websocket/{job_id}")
async def websocket_endpoint(
    job_id: uuid.UUID,
    websocket: WebSocket
    # user: User = Depends(fastapi_users.get_current_user)
):
    await websocket.accept()

    amqp_queue = app.queues.get(job_id)

    if amqp_queue is None:
        message = 'Queue does not exist'
    else:
        async with amqp_queue.iterator() as queue_iter:
            async for message in queue_iter:
                async with message.process():
                    decoded = orjson.loads(message.body)
                    app.queues[job_id] = None
                    break
        
        ratio = len([d for d in decoded if d['success']]) / len(decoded) if decoded else 0
        message = f'task {100.0 * ratio}% done: http://{settings.HOST}:8000/jobs/{job_id}'
    
    await websocket.send_text(message)           
