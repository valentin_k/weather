import settings

def get_feeed_for_task(task_id):
    return f"""
        <!DOCTYPE html>
        <html>
            <head>
                <title>Task Status</title>
            </head>
            <body>
                <h1>Task № {task_id}</h1>
                <ul id='messages'>
                </ul>
                <script>
                    var ws = new WebSocket("ws://{settings.HOST}:8000/result_websocket/{task_id}");
                    ws.onmessage = function(event) {{
                        var messages = document.getElementById('messages')
                        var message = document.createElement('li')
                        var content = document.createTextNode(event.data)
                        message.appendChild(content)
                        messages.appendChild(message)
                    }};
                </script>
            </body>
        </html>
    """
