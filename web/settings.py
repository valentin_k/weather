import os
from secrets import token_urlsafe


POSTGRES_CREDS = {
    'role': os.environ.get('POSTGRES_ROLE', ''),
    'password':  os.environ.get('POSTGRES_PASSWORD', ''),
    'port': 5432,
    'host': 'db',
    'database': 'interview',
}
AMQP_CREDS = {
    'login': 'guest',
    'password': 'guest',
    'host': 'rabbit',
}
SECRET = token_urlsafe(32)
HOST = '127.0.0.1'
