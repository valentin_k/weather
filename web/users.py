from typing import Mapping, Optional, Type

import settings
from common.models import GinoBaseUserTable, db
from common.schemas import User, UserCreate, UserDB, UserUpdate
from fastapi import Request
from fastapi_users import FastAPIUsers
from fastapi_users.authentication import JWTAuthentication
from fastapi_users.db.base import BaseUserDatabase
from fastapi_users.models import UD
from pydantic import UUID4
from sqlalchemy import Table


class GinoUserAdapter(BaseUserDatabase[UD]):
    """
    Database adapter for SQLAlchemy.
    :param user_db_model: Pydantic model of a DB representation of a user.
    :param users: SQLAlchemy users table instance.
    """

    users: Table

    def __init__(
        self,
        user_db_model: Type[UD],
        users: Table,
    ):
        super().__init__(user_db_model)
        self.users = users

    async def get(self, id: UUID4) -> Optional[UD]:
        # query = self.users.select().where(self.users.c.id == id)
        # user = await self.database.fetch_one(query)
        user = await self.users.query.where(
            self.users.id == id
        ).gino.one_or_none()
        return await self._make_user(user.to_dict()) if user else None

    async def get_by_email(self, email: str) -> Optional[UD]:
        user = await self.users.query.where(
            db.func.lower(self.users.email) == db.func.lower(email)
        ).gino.one_or_none()
        return await self._make_user(user.to_dict()) if user else None

    async def create(self, user: UD) -> UD:
        user_dict = user.dict()

        # query = self.users.insert()
        # await self.database.execute(query, user_dict)
        await self.users.create(**user_dict)

        return user

    async def update(self, user: UD) -> UD:
        user_dict = user.dict()

        # query = self.users.update().where(
        #     self.users.c.id == user.id
        # ).values(user_dict)
        # await self.database.execute(query)
        await self.users.update.values(
            user_dict
        ).where(
            self.users.id == user.id
        ).gino.status()

        return user

    async def delete(self, user: UD) -> None:
        # query = self.users.delete().where(self.users.c.id == user.id)
        # await self.database.execute(query)
        await self.users.delete.where(self.users.id == user.id).gino.status()

    async def _make_user(self, user: Mapping) -> UD:
        user_dict = {**user}

        return self.user_db_model(**user_dict)


user_adapter = GinoUserAdapter(
    UserDB,
    GinoBaseUserTable,
)

jwt_authentication = JWTAuthentication(
    secret=settings.SECRET, lifetime_seconds=36000, tokenUrl='/auth/jwt/login'
)

fastapi_users = FastAPIUsers(
    user_adapter,
    [jwt_authentication],
    User,
    UserCreate,
    UserUpdate,
    UserDB
)


def on_after_register(user: UserDB, request: Request):
    print(f'User {user.id} has registered.')


def on_after_forgot_password(user: UserDB, token: str, request: Request):
    print(f'User {user.id} has forgot their password. Reset token: {token}')


def add_user_routes(app):
    app.include_router(
        fastapi_users.get_auth_router(jwt_authentication),
        prefix='/auth/jwt',
        tags=['auth']
    )
    app.include_router(
        fastapi_users.get_register_router(on_after_register),
        prefix='/auth',
        tags=['auth']
    )
    app.include_router(
        fastapi_users.get_reset_password_router(
            settings.SECRET,
            after_forgot_password=on_after_forgot_password
        ),
        prefix='/auth',
        tags=['auth'],
    )
    app.include_router(
        fastapi_users.get_users_router(),
        prefix='/users',
        tags=['users']
    )
