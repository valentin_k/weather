import asyncio
import orjson
from datetime import datetime

import aiohttp
import settings
from aio_pika import connect_robust, Message
from common.models import WheatherFetchJob, WheatherRecord, db
from common.schemas import WheaterRecordAPI, WheaterRecordInternal


class FetchJob():
    def __init__(self, task, job_id):
        self.job_id = job_id
        self.remaining_cities = set(task)

        self.results = []

    def add_error(self, error, city):
        self.results.append({
            'job_id': self.job_id,
            'data': {'error': error},
            'city_name': city,
            'success': False,
            'timestamp': None,
        })

    def add_result(self, data):
        self.results.append({
            'job_id': self.job_id,
            **data,
        })

    async def save_data_if_needed(self, amqp_channel):
        if self.remaining_cities:
            return None

        async with db.transaction():
            await WheatherRecord.insert().gino.all(*self.results)
            await WheatherFetchJob.update.values(
                completed=datetime.now()
            ).where(
                WheatherFetchJob.id == self.job_id
            ).gino.status()
        print('top')
        print(self.results)

        # await asyncio.sleep(10)

        await amqp_channel.default_exchange.publish(
            Message(
                body=orjson.dumps(self.results)
            ),
            routing_key=str(self.job_id)
        )
        print(f'kek {str(self.job_id)}')


class Worker():
    headers = {'content-type': 'application/json'}

    async def _prepare(self):
        await asyncio.sleep(5)
        self.amqp_connection = await connect_robust(
            'amqp://{login}:{password}@{host}/'.format(**settings.AMQP_CREDS),
            loop=asyncio.get_running_loop()
        )
        self.amqp_channel = await self.amqp_connection.channel()
        self.amqp_queue = await self.amqp_channel.declare_queue(
            'fetch_wheather',
            auto_delete=True
        )

        self.session = aiohttp.ClientSession()

        self.gino_engine = await db.set_bind(
            'postgresql://{role}:{password}@{host}:{port}/{database}'.format(
                **settings.POSTGRES_CREDS
            )
        )
        await db.gino.create_all()

        self.queue = asyncio.Queue()

    async def _tear_down(self):
        await self.amqp_channel.close()
        await self.amqp_connection.close()

        await self.session.close()
        await db.pop_bind().close()

    async def fetch_wheather(self, params):
        async with self.session.get(
            settings.FETCH_WEATHER_URL,
            headers=self.headers,
            params=params,
        ) as resp:
            if resp.status != 200:
                raise Exception(f'code {resp.status}')
            data = await resp.json()

        return data

    async def consume(self):
        job_part = await self.queue.get()

        city = job_part['city']
        job = job_part['job']

        try:
            raw = await asyncio.wait_for(
                self.fetch_wheather({
                    'appid': settings.APP_ID,
                    'q': city,
                }),
                timeout=settings.MAX_FETCH_ATTEMPT_DURATION
            )
            WheaterRecordAPI(**raw)
            processed = WheaterRecordInternal(success=True, **raw)
        except asyncio.TimeoutError:
            job.add_error('timeout', city)
        except Exception as e:
            job.add_error(str(e), city)
        else:
            job.add_result(processed.dict())
        finally:
            self.queue.task_done()

            job.remaining_cities.remove(city)
            await job.save_data_if_needed(self.amqp_channel)

    async def consume_infinite(self):
        while True:
            await self.consume()

    async def produce(self):
        async for message in self.amqp_queue:
            with message.process():
                job = FetchJob(**orjson.loads(message.body))

                for city in job.remaining_cities:
                    await self.queue.put({
                        'job': job,
                        'city': city,
                    })

    @classmethod
    def fail_loudly(cls, future):
        if future.exception():
            future.result()

    @classmethod
    async def main(cls):
        worker = cls()
        await worker._prepare()

        producer = asyncio.create_task(worker.produce())
        consumers = [
            asyncio.create_task(worker.consume_infinite())
            for i in range(settings.MAX_CONCURRENT_TASKS_LIMIT)
        ]

        for task in consumers + [producer]:
            task.add_done_callback(cls.fail_loudly)

        await asyncio.gather(producer, *consumers, return_exceptions=True)

        await worker._tear_down()


if __name__ == "__main__":
    asyncio.run(Worker.main())
