pytest==5.*
pytest-asyncio==0.*
aioresponses==0.*
isort==5.*
fastapi-users[sqlalchemy]==3.*
aiohttp==3.*
gino==1.*
pydantic==1.*
aio-pika==6.*
orjson==3.5.*