import os


POSTGRES_CREDS = {
    'role': os.environ.get('POSTGRES_ROLE', ''),
    'password':  os.environ.get('POSTGRES_PASSWORD', ''),
    'port': 5432,
    'host': 'db',
    'database': 'interview',
}
AMQP_CREDS = {
    'login': 'guest',
    'password': 'guest',
    'host': 'rabbit',
}
CHANNEL_NAME = 'worker'
APP_ID = os.environ.get('APP_ID', '')
MAX_FETCH_ATTEMPT_DURATION = 2
FETCH_WEATHER_URL = 'https://api.openweathermap.org/data/2.5/weather'
CITIES = ['London', 'Moscow', 'Kiev', 'Montreal', 'Toronto', 'Paris', 'Saratov', 'Vladimir', 'Madrid', 'Monaco', 'Denpasar', 'New York', 'Tokyo', 'Beijing', 'Mumbai'][:4]

MAX_CONCURRENT_TASKS_LIMIT = 3
