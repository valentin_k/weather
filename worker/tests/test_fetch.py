import asyncio
import json
from unittest.mock import AsyncMock, MagicMock
from urllib.parse import urlencode
from uuid import uuid4

import aiohttp
import pytest
import settings
from aioresponses import aioresponses
from main import FetchJob, Worker


class TestWorker(Worker):
    async def _prepare(self):
        self.session = aiohttp.ClientSession()

    async def _tear_down(self):
        await self.session.close()

    async def fetch_wheather(self, params):
        if params['q'] == 'Toronto':
            await asyncio.sleep(10000)
        return await super().fetch_wheather(params)


@pytest.fixture
def url_generator():
    def _url(city):
        return settings.FETCH_WEATHER_URL + '?' + urlencode({
            'appid': settings.APP_ID,
            'q': city,
        })
    return _url


@pytest.fixture
async def worker_job_generator():
    async def _generator(city):
        worker = TestWorker()
        await worker._prepare()
        worker.gino_engine = MagicMock()
        worker.queue = AsyncMock()

        job = FetchJob(
            [city],
            str(uuid4())
        )
        job.save_data_if_needed = AsyncMock()

        worker.queue.get.return_value = {
            'job': job,
            'city': city,
        }
        worker.queue.task_done.return_value = None

        return worker, job
    return _generator


@pytest.fixture
def http_data():
    with open('tests/fixtures/web_responses.json', 'r') as fixture_file:
        fixtures = json.load(fixture_file)
    return fixtures


@pytest.mark.asyncio
async def test_fetch_ok(
    event_loop,
    url_generator,
    worker_job_generator,
    http_data
):
    with aioresponses() as mocked_http:
        # Лондон грузится нормально
        city = 'London'
        mocked_http.get(
            url_generator(city),
            status=200,
            payload=http_data[city]
        )

        worker, job = await worker_job_generator(city)

        await worker.consume()

        worker.queue.get.assert_awaited()
        worker.queue.task_done.assert_called()
        job.save_data_if_needed.assert_awaited()

    await worker._tear_down()

    assert len(job.results) == 1
    assert {
        # 'job_id': 'f5b164e5-3f50-44df-a90a-09538a021b70',
        'city_name': 'London',
        'success': True,
        # 'timestamp': datetime.datetime(2020, 7, 28, 8, 52, 25),
        'data': {
            'temperature': 290,
            'pressure': 1013,
            'wind_speed': 6,
            'wind_direction': 280
        }
    }.items() <= job.results[0].items()


@pytest.mark.asyncio
async def test_fetch_403(
    event_loop,
    url_generator,
    worker_job_generator,
    http_data
):
    with aioresponses() as mocked_http:
        # Москва на карантине
        city = 'Moscow'

        mocked_http.get(
            url_generator(city),
            status=403,
            payload=http_data[city]
        )

        worker, job = await worker_job_generator(city)

        await worker.consume()

        worker.queue.get.assert_awaited()
        worker.queue.task_done.assert_called()
        job.save_data_if_needed.assert_awaited()

    await worker._tear_down()

    assert len(job.results) == 1
    assert {
        # 'job_id': '344817bf-f887-4b24-9579-2e7ac94063e4',
        'data': {'error': 'code 403'},
        'city_name': 'Moscow',
        'success': False
    }.items() <= job.results[0].items()


@pytest.mark.asyncio
async def test_wrong_data_format(
    event_loop,
    url_generator,
    worker_job_generator,
    http_data
):
    with aioresponses() as mocked_http:
        # в Киевской фикстуре неправильный формат данных ломает pydantic
        city = 'Kyiv'

        mocked_http.get(
            url_generator(city),
            status=200,
            payload=http_data[city]
        )

        worker, job = await worker_job_generator(city)

        await worker.consume()

        worker.queue.get.assert_awaited()
        worker.queue.task_done.assert_called()
        job.save_data_if_needed.assert_awaited()

    await worker._tear_down()

    assert len(job.results) == 1
    assert {
        # 'job_id': '34845b56-e5fe-494e-93e3-72a262ba59b8',
        'city_name': 'Kyiv',
        'success': False,
    }.items() <= job.results[0].items()
    assert '2 validation errors for WheaterRecordAPI' in job.results[0]['data']['error']


@pytest.mark.asyncio
async def test_timeout(
    event_loop,
    url_generator,
    worker_job_generator,
    http_data
):
    with aioresponses() as mocked_http:
        # Торонто зависнет и ничего не вернет
        city = 'Toronto'

        mocked_http.get(
            url_generator(city),
            status=200,
            payload=http_data[city]
        )

        worker, job = await worker_job_generator(city)

        await worker.consume()

        worker.queue.get.assert_awaited()
        worker.queue.task_done.assert_called()
        job.save_data_if_needed.assert_awaited()

    await worker._tear_down()

    assert len(job.results) == 1
    assert {
        # 'job_id': '34845b56-e5fe-494e-93e3-72a262ba59b8',
        'data': {'error': 'timeout'},
        'city_name': 'Toronto',
        'success': False,
    }.items() <= job.results[0].items()
